module PID (controller, PID (..), p, i, d) where

import Control.Concurrent (MVar, forkIO, newEmptyMVar, takeMVar)
import Control.Concurrent.STM (TVar, atomically, newEmptyTMVarIO, newTVarIO, readTVar, readTVarIO, retry, writeTVar)
import Control.Monad (forever)
import Optics
import Streamly.Prelude (IsStream, SerialT, repeatM)

-- https://gist.github.com/gatlin/e3523ae9a026408b11620366ef1be2ae
-- https://github.com/gatlin/tubes/blob/master/Tubes/Core.hs

data PID = PID {_p :: Double, _i :: Double, _d :: Double} deriving (Eq)

makeLenses ''PID

controller :: IsStream t => TVar PID -> TVar Double -> TVar Double -> IO (t IO Double)
controller pid desired measured = do
  tvar <- newTVarIO 0
  pid0 <- readTVarIO pid
  desired0 <- readTVarIO desired
  measured0 <- readTVarIO measured
  loop tvar True 0.0 0.0 pid0 desired0 measured0 & forkIO
  readTVarIO tvar & repeatM & return
  where
    loop :: TVar Double -> Bool -> Double -> Double -> PID -> Double -> Double -> IO ()
    loop tvar isFirst !integral !error !pid0 !desired0 !measured0 = do
      (pid', desired', measured') <- atomically do
        desired1 <- readTVar desired
        measured1 <- readTVar measured
        pid1 <- readTVar pid
        if (pid0, desired0, measured0) == (pid1, desired1, measured1) && not isFirst
          then retry
          else return (pid1, desired1, measured1)
      let error' = measured' - desired'
          integral' = integral + error'
          derivative' = error' - error
          !output = -error' * (pid' ^. p) - integral' * (pid' ^. i) - derivative' * (pid' ^. d)
      atomically $ writeTVar tvar output
      loop tvar False integral' error' pid' desired' measured'
