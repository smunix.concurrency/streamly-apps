module Main where

import Control.Concurrent (forkIO, killThread, threadDelay)
import Control.Concurrent.STM (atomically, modifyTVar', newTVarIO, readTVarIO, writeTVar)
import Data.Functor (($>))
import Data.IORef (IORef, newIORef, readIORef, writeIORef)
import Optics
import PID
import SDL (Event (eventPayload), EventPayload (MouseMotionEvent, WindowClosedEvent), MouseMotionEventData (MouseMotionEventData), Point (P), Rectangle (Rectangle), Renderer, V2 (V2), V4 (V4), Window, createRenderer, createWindow, defaultRenderer, defaultWindow, destroyWindow, fillRect, initializeAll, present, rendererDrawColor, ticks, waitEvent, ($=))
import Streamly.Data.Fold (drainBy)
import Streamly.Prelude (constRate, delay, drain, drainWhile, fromAsync, mapM, repeatM, tap)
import Prelude hiding (init, mapM)

speed :: Double
speed = 6

radius :: Double
radius = 60

{- SDL initialization boilerplate step -}
init :: IO (Window, Renderer)
init = do
  initializeAll
  w <- createWindow ("Circling square") defaultWindow
  r <- createRenderer w (-1) defaultRenderer
  return (w, r)

{- Draw in the renderer -}
display :: (Double, Double) -> Renderer -> IO ()
display (round -> x, round -> y) renderer = do
  -- paint background color
  rendererDrawColor renderer $= V4 55 60 64 255
  fillRect renderer Nothing
  -- paint small square at (x,y) pos
  rendererDrawColor renderer $= V4 212 108 73 255
  fillRect renderer $ Just $ Rectangle (P (V2 x y)) 15

{- Periodically refresh the output display -}
update :: IORef (Double, Double) -> Renderer -> IO ()
update ref renderer = do
  ((/ 1000) . (* speed) . fromIntegral -> t) <- ticks
  (x, y) <- readIORef ref
  print (t, x, y)
  display (x + cos t * radius, y + sin t * radius) renderer
  present renderer

{- Wait and update controller position if it changes -}
events :: IORef (Double, Double) -> IO Bool
events ref = do
  print ("event")
  waitEvent <&> eventPayload >>= \case
    MouseMotionEvent (MouseMotionEventData _ _ _ (P (V2 (fromIntegral -> x) (fromIntegral -> y))) _) -> writeIORef ref (x, y) $> True
    WindowClosedEvent _ -> return False
    _ -> return True

dt :: Double
dt = 2

main :: IO ()
main = do
  pid <- newTVarIO (PID 0.5 0.5 0.5)
  target <- newTVarIO 0
  measured <- newTVarIO 0
  controls <- controller pid target measured
  controls & mapM (atomically . modifyTVar' measured . (+)) & fromAsync & constRate (dt) & drain & forkIO
  (atomically do modifyTVar' target (+ 0)) & repeatM & fromAsync & constRate (dt) & drain & forkIO
  (do getLine <&> read >>= \tgt -> atomically do writeTVar target tgt) & repeatM & fromAsync & drain & forkIO
  (do (,) <$> readTVarIO measured <*> readTVarIO target >>= print) & repeatM & delay (1 / dt) & drain

{-
  (window, renderer) <- init
  ref <- newIORef (0, 0)
  updateThread <- repeatM (update ref renderer) & delay (1 / 60) & drain & forkIO
  eventsThread <- repeatM (events ref) & drainWhile (== True) & forkIO
  threadDelay (10 * 1_000_000)
  destroyWindow window
-}
