import Control.Concurrent (ThreadId, myThreadId, threadDelay)
import Data.Functor (($>))
import Data.Text (Text)
import GHC.Clock (getMonotonicTime)
import Optics
import Streamly.Prelude (IsStream ((|:)), drain, fromAhead, fromAsync, fromSerial, nil, toList)

p :: Int -> IO Int
p d = do myThreadId >>= print . (,d); threadDelay (d * 1_000_000) $> d

perf :: Text -> IO a -> ((Text, ThreadId, Double, a) -> IO r) -> IO r
perf t m kont = do
  th <- myThreadId
  t0 <- getMonotonicTime
  a <- m
  t1 <- getMonotonicTime
  print (th, t, t1 - t0)
  kont (t, th, t1 - t0, a)

main :: IO ()
main = do
  perf "serial" (p 3 |: p 2 |: nil & fromSerial & toList) print
  perf "ahead" (p 3 |: p 2 |: nil & fromAhead & toList) print
  perf "async" (p 3 |: p 2 |: nil & fromAsync & toList) print
