module Main where 

import Control.Concurrent (ThreadId, forkIO, killThread, myThreadId, newEmptyMVar, putMVar, takeMVar, threadDelay)
import Control.Concurrent.STM (TVar, atomically, newTVar, newTVarIO, readTVar, retry, writeTVar)
import Control.Monad (forever, void, (>=>))
import Control.Monad.IO.Class (MonadIO (liftIO))
import Data.Functor (($>))
import Data.Monoid (Sum (Sum, getSum))
import Data.Text (Text)
import GHC.Clock (getMonotonicTime)
import Optics
import Streamly.Data.Fold (drainBy)
import Streamly.Prelude (IsStream, SerialT, drain, fold, fromAsync, fromParallel, iterateM, map, mapM, repeatM, scanl', take)
import Prelude hiding (map, mapM, take)

streamTV :: (a -> b -> a) -> SerialT IO b -> TVar a -> IO ()
streamTV f s tv = do
  i <- atomically $ readTVar tv
  scanl' f i s & fold (drainBy (atomically . writeTVar tv)) & forkIO & void

tvStream :: Eq a => TVar a -> IO (SerialT IO a)
tvStream v = do
  mvar <- newEmptyMVar
  prev <- atomically (readTVar v >>= newTVar)
  isFirst <- newTVarIO True
  forkIO . forever $ do
    val' <- atomically do
      vVal <- readTVar v
      prevVal <- readTVar prev
      isFirstVal <- readTVar isFirst
      case (prevVal == vVal, isFirstVal) of
        (True, False) -> retry
        _ -> do
          writeTVar isFirst False
          writeTVar prev vVal
          return vVal
    putMVar mvar val'
  return $ repeatM (takeMVar mvar)

{-
                ┌──────┐
    ┌───────────┤ Exter◄───────────┐
    │           └──────┘           │
    │                              │
    │                              │
    │                              │
┌───▼───┐ ◄────────────────── ┌────┴─────┐
│ State │                     │ Action   │
└───────┘ ───────────────────►└──────────┘
-}

----------------
-- System API --
----------------

run ::
  (Eq state, Monoid action, Eq action) =>
  SerialT IO external ->
  TVar state ->
  Setter' state external ->
  Lens' state action ->
  Getter action (IO ()) ->
  IO ()
run
  externals
  state
  external
  action
  io = do
    -- external commands update the state
    streamTV (\s u -> s & external .~ u) externals state

    -- states stream
    actions <- tvStream state <&> map (view action)

    -- actions TVar, to copy the actions stream (initial empty)
    actionsTV <- newTVarIO mempty
    streamTV (<>) actions actionsTV

    -- actions for state updates
    states <- tvStream actionsTV
    -- actions for external executions
    ios <- tvStream actionsTV

    -- apply actions to state
    streamTV (\s a -> s & action .~ a) states state

    -- execute actions for their side effects to the external world
    ios & mapM (view io) & drain

--------------------
-- banking system --
--------------------
data State = Account Int deriving (Eq, Show)

data Action

data External
  = Deposit Int
  | Withdraw Int
  deriving (Eq, Show)

ie :: Iso' External Int
ie = iso e2i i2e
  where
    i2e :: Int -> External
    i2e tot
      | tot < 0 = Withdraw (negate tot)
      | otherwise = Deposit tot

    e2i :: External -> Int
    e2i (Deposit i) = i
    e2i (Withdraw i) = negate i

deposit :: Int -> External
deposit x | x > 0 = review ie x

withdraw :: Int -> External
withdraw x | x > 0 = review ie (negate x)

instance Semigroup External where
  Deposit (Sum -> a) <> Deposit (Sum -> b) = a <> b & getSum & review ie
  Deposit (Sum -> a) <> Withdraw (Sum . negate -> b) = a <> b & getSum & review ie
  Withdraw (Sum . negate -> a) <> Withdraw (Sum . negate -> b) = a <> b & getSum & review ie
  a@Withdraw {} <> b@Deposit {} = b <> a

instance Monoid External where
  mempty = Deposit 0

-- >>> deposit 5 <> withdraw 6
-- Withdraw 1

external :: Setter' State External
external = sets \f (Account i) -> Account ((f (review ie 0) <> (review ie i)) & view ie)

-- >>> Account 10 & external .~ (deposit 1) & external .~ (withdraw 2)
-- Account 9

action :: Lens' State Action
action = lens g s
  where
    s :: State -> Action -> State
    s st act = undefined

    g :: State -> Action
    g st = undefined

----------
-- Main --
----------
main :: IO ()
main = do
  t0 <- getMonotonicTime
  iterateM (return . succ) (pure 0)
    & take 1_000_000_000
    & fromAsync
    & drain
  t <- getMonotonicTime
  print (t - t0)
