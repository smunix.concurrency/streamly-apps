module Main where

import Control.Monad (void)
import Control.Monad.IO.Class (MonadIO (liftIO))
import Control.Monad.State (MonadState, StateT (runStateT))
import Data.Functor (($>))
import Optics (equality', use, (&))
import Optics.State.Operators ((%=))
import Streamly.Prelude (MonadAsync, SerialT, constRate, drainWhile, fromAsync, parallel, repeatM)
import Streamly.Prelude qualified as S

data Event = Quit | Harm Int | Potion Int
  deriving (Eq, Show)

withEvent :: r -> (Int -> r) -> (Int -> r) -> Event -> r
withEvent quit harm potion = \case
  Quit -> quit
  Harm (harm -> r) -> r
  Potion (potion -> r) -> r

data Result = Check | Done
  deriving (Eq, Show)

withResult :: r -> r -> Result -> r
withResult c d = \case Check -> c; Done -> d

userEvent :: forall m. (MonadAsync m) => SerialT m Event
userEvent = repeatM askUser
  where
    askUser :: m Event
    askUser =
      liftIO getLine >>= \case
        "p" -> return $ Potion 10
        "h" -> return $ Harm 10
        "q" -> return Quit
        _ -> do
          liftIO $ putStrLn "Type 'p' (Potion), 'h' (Harm) or 'q' (Quit)"
          askUser

rainEvent :: (MonadAsync m) => SerialT m Event
rainEvent =
  repeatM (return $ Harm 1)
    & constRate 1
    & fromAsync

result :: forall m. (MonadAsync m, MonadState Int m) => SerialT m Result
result =
  userEvent `parallel` rainEvent
    >>= withEvent
      (return Done)
      (\harm -> (equality' %= (subtract harm)) $> Check)
      (\potion -> (equality' %= (+ potion)) $> Check)

data Status = Alive | GameOver deriving (Eq, Show)

main :: IO ()
main = result & S.mapM status & drainWhile (/= GameOver) & flip runStateT 60 & void
  where
    status :: Result -> StateT Int IO Status
    status =
      withResult
        ( use equality' >>= \h ->
            if h <= 0
              then do
                liftIO $ putStrLn "You're dead ..."
                return GameOver
              else do
                liftIO $ putStrLn $ "Health (" <> show h <> ")"
                return Alive
        )
        ( do
            liftIO $ putStrLn "Game is Over"
            return GameOver
        )
