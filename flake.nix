{
  description = "Streamly based apps";

  inputs.nixpkgs.url = "github:nixos/nixpkgs?ref=nixos-unstable";
  inputs.devshell.url = "github:numtide/devshell";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.nix-filter.url = "github:numtide/nix-filter";
  inputs.streamly.url = "github:composewell/streamly";
  inputs.streamly.flake = false;

  outputs = { self, flake-utils, nix-filter, devshell, nixpkgs, streamly }:
    with flake-utils.lib;
    with nix-filter.lib;
    eachSystem [ "x86_64-linux" ] (system:
      let
        version = "";
        config = { };
        ghc = "ghc921";
        overlay = f: p:
          with f;
          let
            haskellPackages = f.haskell.packages.${ghc}.override {
              overrides = hf: hp:
                with haskell.lib; {
                  streamly-apps = with hf;
                    disableLibraryProfiling (addBuildTools
                      (callCabal2nix "streamly-apps" (filter {
                        root = self;
                        exclude = [ (matchExt "cabal") ];
                      }) { }) [ hpack ]);
                  streamly = dontCheck (dontHaddock (disableLibraryProfiling
                    (hf.callCabal2nix "streamly" (filter { root = streamly; })
                      { })));
                  sdl2 = dontCheck
                    (dontHaddock (disableLibraryProfiling (hp.sdl2_2_5_3_1)));
                  linear = dontCheck
                    (dontHaddock (disableLibraryProfiling (hp.linear_1_21_8)));
                  optics = dontCheck
                    (dontHaddock (disableLibraryProfiling (hp.optics_0_4)));
                  optics-extra = dontCheck (dontHaddock
                    (disableLibraryProfiling (hp.optics-extra_0_4)));
                };
            };
          in { inherit haskellPackages; };
        pkgs = import nixpkgs {
          inherit system config;
          overlays = [ devshell.overlay overlay ];
        };
      in with pkgs.lib; rec {
        inherit overlay;
        packages = flattenTree
          (recurseIntoAttrs { inherit (pkgs.haskellPackages) streamly-apps; });
        defaultPackage = packages.streamly-apps;
        apps = {
          tuto-generating = mkApp {
            drv = defaultPackage;
            exePath = "/bin/tuto-generating";
          };
          tuto-mapping = mkApp {
            drv = defaultPackage;
            exePath = "/bin/tuto-mapping";
          };
          acid-rain = mkApp {
            drv = defaultPackage;
            exePath = "/bin/acid-rain";
          };
          circling-square = mkApp {
            drv = defaultPackage;
            exePath = "/bin/circling-square";
          };
        };
        defaultApp = apps.acid-rain;
        devShell = with pkgs;
          pkgs.devshell.mkShell {
            imports = [ (pkgs.devshell.importTOML ./devshell.toml) ];
            packages = [
              haskellPackages.cabal-install
              haskellPackages.sdl2
              pkg-config
              SDL2
              SDL2.dev
            ];
            env = [{
              name = "PKG_CONFIG_PATH";
              value = "${SDL2.dev}/lib/pkgconfig";
            }];
            commands = [
              {
                name = "cabal-v2-configure";
                category = "cabal";
                help = "generate cabal.project.local file";
                command = "cabal v2-configure --extra-lib-dirs ${SDL2}/lib";
              }
              {
                name = "run-tuto-generating";
                category = "apps";
                help = "run generating streams (concurrently)";
                command = "${packages.streamly-apps}/bin/tuto-generating";
              }
              {
                name = "run-tuto-mapping";
                category = "apps";
                help = "run mapping streams (concurrently)";
                command = "${packages.streamly-apps}/bin/tuto-mapping";
              }
              {
                name = "run-acid-rain";
                category = "apps";
                help = "run acid rain";
                command = "${packages.streamly-apps}/bin/acid-rain";
              }
              {
                name = "run-circling-square";
                category = "apps";
                help = "run acid rain";
                command = ''
                  ${xvfb_run}/bin/xvfb-run ${packages.streamly-apps}/bin/circling-square
                '';
              }
            ];
          };
      });
}
